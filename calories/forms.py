from django import forms
from allauth.account.forms import SignupForm, LoginForm, AddEmailForm
from django.utils.safestring import mark_safe
from calories.models import Calorie


class form_field_attr(object):
    def __init__(self, field, attrs):
        self.field = field
        self.attrs = attrs

    def __call__(self, cls):
        field = self.field
        attrs = self.attrs

        class Wrapped(cls):
            def __init__(self, *args, **kwargs):
                super(Wrapped, self).__init__(*args, **kwargs)
                for k, v in attrs:
                    self.fields[field].widget.attrs[k] = v
        return Wrapped


@form_field_attr('login', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your username'),))
@form_field_attr('password', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your password'),))
class LoginForm(LoginForm):
    pass


@form_field_attr('username', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your username'),))
@form_field_attr('password1', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your password'),))
@form_field_attr('password2', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Confirm your password'),))
@form_field_attr('email', (('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your email'),('type', 'email'),))
class SignupForm(SignupForm):
    pass


@form_field_attr('username', (('value', mark_safe('<%- username %>')), ('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your username'),))
@form_field_attr('email', (('value', mark_safe('<%- email %>')), ('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your email'),))
@form_field_attr('first_name', (('value', mark_safe('<%- first_name %>')), ('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your first name'),))
@form_field_attr('last_name', (('value', mark_safe('<%- last_name %>')), ('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter your last name'),))
@form_field_attr('max_calories_per_day', (('value', mark_safe('<%- max_calories_per_day %>')), ('class', 'form-control'), ('required', 'required'), ('placeholder', 'Enter expected number of calories per day'), ))
class UserForm(AddEmailForm):
    username = forms.CharField(max_length=255)
    first_name = forms.CharField(max_length=255)
    last_name = forms.CharField(max_length=255)
    max_calories_per_day = forms.IntegerField(min_value=0, label='Expected calories per day')


@form_field_attr('date', (('value', mark_safe('<%- date %>')), ('class', 'form-control'), ('required', 'required'),))
@form_field_attr('time', (('value', mark_safe('<%- time %>')), ('class', 'form-control'), ('required', 'required'),))
@form_field_attr('text', (('value', mark_safe('<%- text %>')), ('class', 'form-control'), ('required', 'required'),))
@form_field_attr('calories', (('value', mark_safe('<%- calories %>')), ('class', 'form-control'), ('required', 'required'),))
class CalorieForm(forms.ModelForm):
    class Meta:
        model = Calorie
        fields = ['date', 'time', 'text', 'calories']
        labels = {
            'text': 'Comment'
        }
        widgets = {
            'date': forms.DateInput({'type': 'date'}),
            'time': forms.TimeInput({'type': 'time'})
        }


@form_field_attr('date_from', (('class', 'form-control'),))
@form_field_attr('date_to', (('class', 'form-control'),))
@form_field_attr('time_from', (('class', 'form-control'),))
@form_field_attr('time_to', (('class', 'form-control'),))
class FilterForm(forms.Form):
    date_from = forms.DateField(required=False, widget=forms.DateInput({'type': 'date'}))
    date_to = forms.DateField(required=False, label="to", widget=forms.DateInput({'type': 'date'}))
    time_from = forms.TimeField(required=False, widget=forms.DateInput({'type': 'time'}))
    time_to = forms.TimeField(required=False, label="to", widget=forms.DateInput({'type': 'time'}))
