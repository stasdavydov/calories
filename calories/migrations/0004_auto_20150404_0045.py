# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calories', '0003_auto_20150402_1431'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='calorie',
            options={'ordering': ('date', 'time')},
        ),
    ]
