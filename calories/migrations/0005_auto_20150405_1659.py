# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calories', '0004_auto_20150404_0045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calorie',
            name='calories',
            field=models.PositiveIntegerField(db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='calorie',
            name='date',
            field=models.DateField(db_index=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='calorie',
            name='time',
            field=models.TimeField(db_index=True),
            preserve_default=True,
        ),
    ]
