# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calories', '0002_usersettings'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usersettings',
            name='Max Calories Per Day',
        ),
        migrations.AddField(
            model_name='usersettings',
            name='max_calories_per_day',
            field=models.PositiveIntegerField(default=0),
            preserve_default=True,
        ),
    ]
