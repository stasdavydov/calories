CalorieManager.Views.Calories = Backbone.View.extend({
    template: _.template($('#tpl-calories').html()),

    events: {
        'submit #filter-form': 'onFilterSubmit'
    },

    renderOne: function(calorie) {
        var itemView = new CalorieManager.Views.Calorie({model: calorie});
        this.$('.calories-container').append(itemView.render().$el);
    },

    render: function() {
        var html = this.template();
        this.$el.html(html);
        this.collection.each(this.renderOne, this);
        this.$el.find('#calories-table')
            .stupidtable()
            .on("aftertablesort", function (event, data) {
                var th = $(this).find("th");
                th.find(".glyphicon").remove();
                var dir = $.fn.stupidtable.dir;

                $('<span class="glyphicon"></span>')
                    .addClass(data.direction === dir.ASC ? "glyphicon-sort-by-attributes" : "glyphicon-sort-by-attributes-alt")
                    .appendTo(th.eq(data.column));

                Filters.order_column.set(data.column);
                Filters.order_direction.set(data.direction);
            });

        this.$el.find('#id_date_from').val(Filters.date_from.get(''));
        this.$el.find('#id_date_to').val(Filters.date_to.get(''));
        this.$el.find('#id_time_from').val(Filters.time_from.get(''));
        this.$el.find('#id_time_to').val(Filters.time_to.get(''));

        this.$el.find("thead th").eq(parseInt(Filters.order_column.get(0))).stupidsort(Filters.order_direction.get('asc'));

        return this;
    },
    onFilterSubmit: function(e) {
        e.preventDefault();

        Filters.date_from.set(this.$el.find('#id_date_from').val());
        Filters.date_to.set(this.$el.find('#id_date_to').val());
        Filters.time_from.set(this.$el.find('#id_time_from').val());
        Filters.time_to.set(this.$el.find('#id_time_to').val());
        var me = this;
        this.collection.update(function() {
            me.render();
        });
    }
});