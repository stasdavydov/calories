CalorieManager.Views.Calorie = Backbone.View.extend({
    tagName: 'tr',
    template: _.template($('#tpl-calorie').html()),

    render: function() {
        var html = this.template(this.model.toJSON());
        this.$el.append(html);

        if (this.model.attributes.calories_alert) {
            this.$el
                .addClass('bg-danger')
                .removeClass('bg-success');
        } else {
            this.$el
                .addClass('bg-success')
                .removeClass('bg-danger');
        }
        return this;
    }
});