CalorieManager.Views.UserForm = CalorieManager.Views.BaseForm.extend({
    template: _.template($('#tpl-user').html()),

    render: function() {
        var html = this.template(this.model.toJSON());
        this.$el.append(html);
        return this;
    },

    events: {
        'submit #user-form': 'onFormSubmit'
    },

    onFormSubmit: function(e) {
        e.preventDefault();
        this.clear_messages();

        var attrs = {
            username: this.$('#id_username').val(),
            first_name: this.$('#id_first_name').val(),
            last_name: this.$('#id_last_name').val(),
            email: this.$('#id_email').val(),
            max_calories_per_day: this.$('#id_max_calories_per_day').val(),
            new_password1: this.$('#id_new_password1').val(),
            new_password2: this.$('#id_new_password2').val()
        };
        this.trigger('form:submitted', attrs);
    }

});