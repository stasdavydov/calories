CalorieManager.Views.LoginForm = CalorieManager.Views.BaseForm.extend({
    template: _.template($('#tpl-login').html()),
    render: function() {
        var html = this.template(_.extend(this.model.toJSON(), {
            isNew: this.model.isNew()
        }));
        this.$el.append(html);
        return this;
    },
    events: {
        'submit #login-form': 'onFormSubmit'
    },

    onFormSubmit: function(e) {
        e.preventDefault();

        var attrs = {
            login: this.$('#id_login').val(),
            password: this.$('#id_password').val()
        };
        this.trigger('form:submitted', attrs);
    }
});