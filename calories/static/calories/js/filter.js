var Filter = function (name) {
    return {
        get: function (def) {
            var val = $.cookie(name);
            if (val === undefined && def !== undefined) {
                return def;
            }
            return val;
        },
        set: function (value) {
            $.cookie(name, value, {
                expires: 180,
                path: '/'
            });
        }
    }
};
var Filters = {
    date_from: Filter('date_from'),
    date_to: Filter('date_to'),
    time_from: Filter('time_from'),
    time_to: Filter('time_to'),
    order_column: Filter('order_column'),
    order_direction: Filter('order_dir')
};