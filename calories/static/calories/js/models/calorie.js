var DATE_FORMAT = 'YYYY-MM-DD';
var TIME_FORMAT = 'HH:mm';
var DATETIME_FORMAT = DATE_FORMAT + ' ' + TIME_FORMAT;

var get_datetime = function (date, time) {
    return moment(date + ' ' + time, DATETIME_FORMAT).format('MMM Do, YYYY HH:mm');
};

CalorieManager.Models.Calorie = Backbone.Model.extend({
    urlRoot: '/api/calories/',
    defaults: {
        id: null,
        date: moment().format(DATE_FORMAT),
        time: moment().format(TIME_FORMAT),
        text: '',
        calories: '',
        calories_alert: false
    },
    validation: {
        date: {
            required: true,
            msg: 'Please enter date'
        },
        time: {
            required: true,
            msg: 'Please enter time'
        },
        text: {
            required: true,
            msg: 'Please tell what you ate'
        },
        calories: [
            {
                required: true,
                msg: 'Please enter how many calories was the meal'
            },
            {
                pattern: 'digits',
                min: 0,
                msg: 'Calories should be great or equal to zero'
            }
        ]
    }
})
;