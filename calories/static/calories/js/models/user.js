CalorieManager.Models.NewUser = Backbone.Model.extend({
    urlRoot: '/api/auth/signup',
    defaults: {
        username: null,
        email: null,
        password1: null,
        password2: null
    },
    validation: {
        username: {
            required: true,
            msg: 'Please enter username'
        },
        password1: {
            required: true,
            msg: 'Please enter password'
        },
        password2: {
                required: true,
                equalTo: 'password1',
                msg: 'Passwords doesn\'t match'
        },
        email: {
            required: true,
            msg: 'Please enter email'
        }
    }
});

CalorieManager.Models.LoginUser = Backbone.Model.extend({
    urlRoot: '/api/auth/login',
    defaults: {
        login: null,
        password: null
    },
    validation: {
        login: {
            required: true,
            msg: 'Please enter username'
        },
        password: {
            required: true,
            msg: 'Please enter password'
        }
    }
});

CalorieManager.Models.User = Backbone.Model.extend({
    urlRoot: '/api/user/',
    defaults: {
        username: null,
        email: null,
        first_name: null,
        last_name: null,
        new_password1: null,
        new_password2: null,
        max_calories_per_day: 0
    },
    validation: {
        username: {
            required: true,
            msg: 'Please enter username'
        },
        email: {
            required: true,
            msg: 'Please enter email'
        },
        new_password2: {
            equalTo: 'new_password1',
            msg: 'Passwords doesn\'t match'
        },
        max_calories_per_day: {
            min: 0,
            msg: 'Minimum expected number of calories is 0'
        }
    }

}, {
    get_user: function (success, error) {
        var user = new CalorieManager.Models.User();
        user.fetch({
            success: success,
            error: error
        });
        return user;
    }
});