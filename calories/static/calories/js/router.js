CalorieManager.Router = Backbone.Router.extend({
    routes: {
        '': 'home',
        'login': 'login',
        'logout': 'logout',
        'signup': 'signup',
        'profile': 'profile',
        'calories': 'showCalories',
        'calories/new': 'newCalorie',
        'calories/edit/:id': 'editCalorie',
        'calories/delete/:id': 'deleteCalorie'
    }
});