from datetime import timedelta
import json
from allauth.account.models import EmailAddress
from django.contrib.auth.models import User
from django.utils.timezone import now
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_403_FORBIDDEN, HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT
from rest_framework.test import APILiveServerTestCase, APIClient
from calories.models import UserSettings
from calories.tests.const import *


class RESTCalorieTestCase(APILiveServerTestCase):
    def setUp(self):
        self.user = User.objects.create_user(TEST_USERNAME1, email=TEST_EMAIL1, password=TEST_PASSWORD1)
        UserSettings(user=self.user, max_calories_per_day=MAX_CALORIES_PER_DAY).save()
        EmailAddress(user=self.user, email=TEST_EMAIL1, verified=True, primary=True).save()

    def test_unauthorized_access(self):
        url = reverse('calorie-list')
        response = self.client.get(url, format='json')
        self.assertEqual(HTTP_403_FORBIDDEN, response.status_code, "Wrong access status, should be 403")

    def test_login(self):
        url = reverse('rest_login')
        response = self.client.post(url, {'username': TEST_USERNAME1, 'password': TEST_PASSWORD1})
        self.assertEqual(HTTP_200_OK, response.status_code, "Wrong login status, should be 200")

    def test_signup(self):
        data = {'username': TEST_USERNAME2, 'password1': TEST_PASSWORD2, 'password2': TEST_PASSWORD2, 'email': TEST_EMAIL2 }
        response = self.client.post('/api/auth/signup/', data)
        self.assertEqual(HTTP_201_CREATED, response.status_code, "Wrong signup status, should be 201")
        self.assertJSONEqual(response.content, {'username': TEST_USERNAME2, 'email': TEST_EMAIL2, 'first_name': '', 'last_name': ''}, "Wrong signup response")

    def test_calorie_list(self):
        client = APIClient()
        client.force_authenticate(self.user)

        response = client.get(reverse('calorie-list'))
        self.assertJSONEqual(response.content, [], "Calorie list should be empty")

    def assertCalorieEqual(self, c1, c2):
        self.assertEqual(c1['date'], c2['date'], "Wrong date field")
        self.assertEqual(c1['time'][:5], c2['time'][:5], "Wrong time field: ")
        self.assertEqual(c1['text'], c2['text'], "Wrong text field")
        self.assertEqual(c1['calories'], c2['calories'], "Wrong calories field")

    def test_add_edit_delete_calorie(self):
        client = APIClient()
        client.force_authenticate(self.user)

        # add
        calorie = {'date': '2015-04-06', 'time': '10:00', 'text': 'Apple', 'calories': 100}
        response = client.post(reverse('calorie-list'), data=calorie)
        res = json.loads(response.content)
        self.assertCalorieEqual(calorie, res)
        self.assertTrue('id' in res)

        calorie_id = res['id']

        # edit
        calorie['text'] = 'Melon'
        response = client.put(reverse('calorie-detail', (calorie_id, )), data=calorie)
        res = json.loads(response.content)
        self.assertCalorieEqual(calorie, res)

        # delete
        response = client.delete(reverse('calorie-detail', (calorie_id, )))
        self.assertEqual(HTTP_204_NO_CONTENT, response.status_code)

    def test_calorie_alert(self):
        client = APIClient()
        client.force_authenticate(self.user)

        today = now()

        # Eat an apple
        calorie = {'date': '2015-04-06', 'time': '10:00', 'text': 'Apple', 'calories': 100}
        response = client.post(reverse('calorie-list'), data=calorie)
        apple = json.loads(response.content)

        response = client.get(reverse('calorie-list'))
        res = json.loads(response.content)
        self.assertTrue(len(res) == 1, "Wrong calorie list count")
        self.assertCalorieEqual(apple, res[0])
        self.assertEqual(0, res[0]['calories_alert'], "Wrong alert") # no alert

        today = today + timedelta(minutes=1)
        # eat big pie
        calorie = {'date': '2015-04-06', 'time': '10:00', 'text': 'Big Pie', 'calories': MAX_CALORIES_PER_DAY+1}
        response = client.post(reverse('calorie-list'), data=calorie)
        big_pie = json.loads(response.content)

        response = client.get(reverse('calorie-list'))
        res = json.loads(response.content)
        self.assertTrue(len(res) == 2, "Wrong calorie list count")
        self.assertCalorieEqual(big_pie, res[1])
        self.assertEqual(1, res[0]['calories_alert'], "Wrong alert") # alert
        self.assertEqual(1, res[1]['calories_alert'], "Wrong alert") # alert

        # increase alert border
        self.user.usersettings.max_calories_per_day = apple['calories'] + big_pie['calories']
        self.user.usersettings.save()

        response = client.get(reverse('calorie-list'))
        res = json.loads(response.content)
        self.assertEqual(0, res[0]['calories_alert'], "Wrong alert") # alert
        self.assertEqual(0, res[1]['calories_alert'], "Wrong alert") # alert

